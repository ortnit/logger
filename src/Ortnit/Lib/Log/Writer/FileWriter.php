<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 25.12.17
 * Time: 08:45
 */

namespace Ortnit\Lib\Log\Writer;


use Ortnit\Lib\Log\WriterInterface;

class FileWriter implements WriterInterface
{
    protected $_handler = null;

    public function __construct($fileName)
    {
        $this->_handler = fopen($fileName, 'a');
    }

    /**
     * writes log output to some place
     *
     * @param $message
     */
    public function send($message)
    {
        fwrite($this->_handler, $message);
    }


}