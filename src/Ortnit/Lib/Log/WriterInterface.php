<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 25.12.17
 * Time: 08:43
 */

namespace Ortnit\Lib\Log;


interface WriterInterface
{

    /**
     * writes log output to some place
     *
     * @param $message
     */
    public function send($message);
}