<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 25.12.17
 * Time: 13:37
 */

namespace Ortnit\Lib\Log;


use Ortnit\Lib\Log\Writer\FileWriter;

class FileLogger extends OutputLogger
{
    public function __construct($fileName)
    {
        $this->_writer = new FileWriter($fileName);
    }
}