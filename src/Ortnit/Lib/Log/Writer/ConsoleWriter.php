<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 25.12.17
 * Time: 08:45
 */

namespace Ortnit\Lib\Log\Writer;


use Ortnit\Lib\Log\WriterInterface;

class ConsoleWriter implements WriterInterface
{
    /**
     * writes log output to some place
     *
     * @param $message
     */
    public function send($message)
    {
        echo $message;
    }


}