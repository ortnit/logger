<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 23.12.17
 * Time: 23:10
 */

namespace Ortnit\Lib\Log;


use Ortnit\Lib\Log\Writer\ConsoleWriter;

class ConsoleLogger extends OutputLogger
{
    public function __construct()
    {
        $this->_writer = new ConsoleWriter();
    }
}