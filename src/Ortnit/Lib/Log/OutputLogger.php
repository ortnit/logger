<?php
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 25.12.17
 * Time: 13:31
 */

namespace Ortnit\Lib\Log;


use Carbon\Carbon;
use Psr\Log\AbstractLogger;

abstract class OutputLogger extends AbstractLogger
{
    /**
     * @var WriterInterface
     */
    protected $_writer = null;

    /**
     * get the current timestamp
     *
     * @return string
     */
    protected function _now() {
        return Carbon::now();
    }

    protected function _send($message) {
        $message = trim($message);
        $this->_writer->send(sprintf("[%s]%s\n", $this->_now(), $message));
    }

    /**
     * @param string $message
     * @param array $context
     * @return \Exception
     */
    protected function _getException(string $message, array &$context) {
        if(isset($context['exception']) && $context['exception'] instanceof \Exception) {
            $e = $context['exception'];
            unset($context['exception']);
        } else {
            $e = new \Exception($message);
        }
        return $e;
    }

    protected function _getFunctionName() {
        $trace=debug_backtrace();
        $name = $trace[2]['function'] ?? 'default';
        return $name;
    }

    protected function _simpleLog(string $message) {
        $functionName = $this->_getFunctionName();
        $message = sprintf('[%s] %s', strtoupper($functionName), $message);
        $this->_send($message);
    }

    protected function _complexLog(string $message, array $context = []) {
        $functionName = $this->_getFunctionName();
        $message = sprintf('[%s] %s', strtoupper($functionName), $message);

        $e = $this->_getException($message, $context);
        $message .= "\n" . $e->getTraceAsString();
        if(!empty($context)) {
            $message .= "\n" . json_encode($context, JSON_PRETTY_PRINT);
        }
        $this->_send($message);
    }

    public function alert(string $message, array $context = [])
    {
        $this->_complexLog($message, $context);
    }

    public function emergency(string $message, array $context = [])
    {
        $this->_complexLog($message, $context);
    }

    public function critical(string $message, array $context = [])
    {
        $this->_complexLog($message, $context);
    }

    public function error(string $message, array $context = [])
    {
        $this->_complexLog($message, $context);
    }

    public function warning(string $message, array $context = [])
    {
        $this->_simpleLog($message);
    }

    public function notice(string $message, array $context = []) {
        $this->_simpleLog($message);
    }


    public function info(string $message, array $context = []) {
        $this->_simpleLog($message);
    }


    public function debug(string $message, array $context = [])
    {
        $this->_complexLog($message, $context);
    }
}